Sets
*Below I have defined the sets we are going to work with. The set S is the players available,
*the set P represents the possible positions, and the set F contains acceptable formations.
    s 'players' / S1*S25 /
    p 'positions' / GK, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW /
    f 'formations' / 442, 352, 4312, 433, 343, 4321 /
    
*From our set S, we create two subsets. The first subset describes which players that are quality players.
*The second subset describes which players that are strength players. 
    subsetqplayers(s) 'subset of quality players' /S13, S20, S21, S22 /
    subsetsplayers(s) 'subset of strength players' /S10, S12, S23 /;

*Now we present a table that describes the players' fitness at the possible positions on the pitch. 
Table F(s,p) Fitness of player s in position p
            GK          CDF         LB          RB          CMF         LW          RW          OMF         CFW         SFW
S1          10          0           0           0           0           0           0           0           0           0    
S2          9           0           0           0           0           0           0           0           0           0
S3          8.5         0           0           0           0           0           0           0           0           0
S4          0           8           6           5           4           2           2           0           0           0         
S5          0           9           7           3           2           0           2           0           0           0
S6          0           8           7           7           3           2           2           0           0           0
S7          0           6           8           8           0           6           6           0           0           0          
S8          0           4           5           9           0           6           6           0           0           0
S9          0           5           9           4           0           7           2           0           0           0
S10         0           4           2           2           9           2           2           0           0           0           
S11         0           3           1           1           8           1           1           4           0           0
S12         0           3           0           2           10          1           1           0           0           0
S13         0           0           0           0           7           0           0           10          6           0
S14         0           0           0           0           4           8           6           5           0           0           
S15         0           0           0           0           4           6           9           6           0           0
S16         0           0           0           0           0           7           3           0           0           0
S17         0           0           0           0           3           0           9           0           0           0
S18         0           0           0           0           0           0           0           6           9           6
S19         0           0           0           0           0           0           0           5           8           7
S20         0           0           0           0           0           0           0           4           4           10
S21         0           0           0           0           0           0           0           3           9           9
S22         0           0           0           0           0           0           0           0           8           8
S23         0           3           1           1           8           4           3           5           0           0
S24         0           3           2           4           7           6           5           6           4           0
S25         0           4           2           2           6           7           5           2           2           0;

*Here we list a table that contains information about the requirements on the positions in a given formation. 
Table R(f,p) Required players for position p for formation f
            GK          CDF         LB          RB          CMF         LW          RW          OMF         CFW         SFW
442         1           2           1           1           2           1           1           0           2           0
352         1           3           0           0           3           1           1           0           1           1           
4312        1           2           1           1           3           0           0           1           2           0        
433         1           2           1           1           3           0           0           0           1           2
343         1           3           0           0           2           1           1           0           1           2
4321        1           2           1           1           3           0           0           2           1           0;

*To solve the problem we will need to define some variables. First of all, we define two binary variables.
*The x-variable indicates if player s is chosen for position p, in formation f. Later we will make sure that
*x(s,p,f) takes the value 1, if player s is chosen for position p, in formation f, and 0 otherwise.
*The y-variable indicates if formation f is chosen. Later we will make sure that the y-variable takes the
*value 1, if formations f is in use, and 0 otherwise.  
Binary Variables
x(s,p,f) indicates if player s is chosen for position p
y(f) indicates if formation f is chosen by the manager;

*The idea of this problem is to maximize the total fitness in the chosen football team. This is done
*by choosing the optimal players for the optional positions in the optimal formation. We define the
*objective function value by the greek letter xi. 
Variables
xi objective function value

*By now we can write down the mathematical formulation of our problem. We begin by giving names to
*the objective function and our constraints, and we furthermore indicate what they depend on. 
Equations
objectivefunction the objective function
maxoneposition(s,f) one player can at max play in one position on the pitch in a formation
exactlyoneformation exactly one formation must be chosen
onlyoneformationhasplayers(f,p) only one formation has players
minimalonequalityplayer(f) a minimal of one quality player must be chosen
qualityandstrength(f) a minimal of one strength player must be chosen is four quality players are chosen;

*The objective function indicates that we would like to maximize the total fitness of the players on the pitch.
*We multiply the player's fitness with the variable indicating, if the player is on the pitch in a given position
*in a given formation. 
*We sum over formations, positions and players. We need to maximize under constraints which is presented
*afterwards.
objectivefunction .. xi =e= sum(f,sum(p,sum(s, F(s,p)*x(s,p,f))));

*The first constraint indicates that a player at max can play in one position in every formation.
maxoneposition(s,f) .. sum(p, x(s,p,f)) =l= 1;

*The second constraint implies that the manager shall pick exactly one formation between a set of formations.
exactlyoneformation .. sum(f, y(f)) =e= 1;

*The third constraint describes that since the manager only can pick one formation, then only one formation
*should have players in each position. The multiplication of y(f) on the right hand side means that we force
*x(s,p,f) to be zero, when f is not the formation the manager has chosen. Since we have a maximization problem,
*every position in the chosen formation will be filled. 
onlyoneformationshasplayers(f,p) .. sum(s, x(s,p,f)) =l= R(f,p)*y(f);

*The fourth constraint implies that the manager has to choose minimum one quality player in his chosen formation.
*in every other formation not selected, there will be none quality players due to the third constraint.
minimalonequalityplayer(f) .. sum(p,sum(subsetqplayers(s), x(s,p,f))) =g= y(f);

*The fifth and final constraint indicates that if the manager chooses all quality players in his given formation,
*that is four quality players, then he needs to choose at least one strength player. The constraint furthermore
*describes, that if the manager chooses between 0-3 quality players, then the he does not need to choose a
*stregnth player, but it is an option.
qualitystrength(f) .. sum(p,sum(subsetqplayers(s), x(s,p,f))) =l= (sum(p,sum(subsetsplayers(s),x(s,p,f)))+4);


*We define the optimization problem by all the previous descriptions.
Model football /all/;

*We solbe the optimization problem by indicating that we need to maximize xi under the given constraints.
Solve football using MIP maximizing xi;

Display xi.l, x.l, y.l;
*We can see that the optimal formation is 4312. For this formation the manager should pick Player 1, 4 ,5, 8, 9,
*10, 11, 12, 13, 18, 21. The optimal objection function value is 100, so the total fitness of the players in
*the optimal scenario is 100. 


