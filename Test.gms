Sets
    s 'players' / S1*S25 /
    p 'positions' / GK, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW /
    f 'formations' / 442, 352, 4312, 433, 343, 4321 /
    subqp(s) 'subset of quality players' /S13, S20, S21, S22 /
    subsp(s) 'subset of strength players' /S10, S12, S23 /;
Display subqp;

Table Q(s,p) quality of player s in position p
            GK          CDF         LB          RB          CMF         LW          RW          OMF         CFW         SFW
S1          10          0           0           0           0           0           0           0           0           0    
S2          9           0           0           0           0           0           0           0           0           0
S3          8.5         0           0           0           0           0           0           0           0           0
S4          0           8           6           5           4           2           2           0           0           0         
S5          0           9           7           3           2           0           2           0           0           0
S6          0           8           7           7           3           2           2           0           0           0
S7          0           6           8           8           0           6           6           0           0           0          
S8          0           4           5           9           0           6           6           0           0           0
S9          0           5           9           4           0           7           2           0           0           0
S10         0           4           2           2           9           2           2           0           0           0           
S11         0           3           1           1           8           1           1           4           0           0
S12         0           3           0           2           10          1           1           0           0           0
S13         0           0           0           0           7           0           0           10          6           0
S14         0           0           0           0           4           8           6           5           0           0           
S15         0           0           0           0           4           6           9           6           0           0
S16         0           0           0           0           0           7           3           0           0           0
S17         0           0           0           0           3           0           9           0           0           0
S18         0           0           0           0           0           0           0           6           9           6
S19         0           0           0           0           0           0           0           5           8           7
S20         0           0           0           0           0           0           0           4           4           10
S21         0           0           0           0           0           0           0           3           9           9
S22         0           0           0           0           0           0           0           0           8           8
S23         0           3           1           1           8           4           3           5           0           0
S24         0           3           2           4           7           6           5           6           4           0
S25         0           4           2           2           6           7           5           2           2           0;

Table b(f,p) demanded players for position p for formation f
            GK          CDF         LB          RB          CMF         LW          RW          OMF         CFW         SFW
442         1           2           1           1           2           1           1           0           2           0
352         1           3           0           0           3           1           1           0           1           1           
4312        1           2           1           1           3           0           0           1           2           0        
433         1           2           1           1           3           0           0           0           1           2
343         1           3           0           0           2           1           1           0           1           2
4321        1           2           1           1           3           0           0           2           1           0;

Binary Variables
x(s,p,f) counts if player s is chosen for position p
y(f) indicates if formation f is chosen;


Variables
xi objective function value

Equations
total objective function
constraint1(s,f) one player can at max take one position
constraint2 exactly one formation
constraint3(f,p) every position in every formation is filled
constraint4(f) minimal one quality player
constraint5(f) minimal one strength player if four quality players;


total .. xi =e= sum(f,sum(p,sum(s, Q(s,p)*x(s,p,f))));
constraint1(s,f) .. sum(p, x(s,p,f)) =l= 1;
constraint4(f) .. sum(p,sum(subqp(s), x(s,p,f))) =g= 1*y(f);
constraint5(f) .. sum(p,sum(subqp(s), x(s,p,f))) =l= (sum(p,sum(subsp(s),x(s,p,f)))+card(subqp));
constraint2 .. sum(f, y(f)) =e= 1;
constraint3(f,p) .. sum(s, x(s,p,f)) =l= b(f,p)*y(f);


Model football /all/;
Solve football using MIP maximizing xi;
Display xi.l, x.l, y.l;



