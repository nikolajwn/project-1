Sets
*Below I have defined the sets we are going to work with. The set V is the ships available,
*the set P represents the ports, and the set R contains routes.
v ships /v1*v5/
p ports /Singapore, Incheon, Shanghai, Sydney, Gladstone, Dalian, Osaka, Victoria/
r route /Asia, ChinaPacific/

*From our set P, we create two subsets. Each subset desribes two ports, that cannot both be serviced by the
*company. Singapore and Osaka cannot both be serviced, an the same goes for Incheon and Victoria. 
subSingaOsaka(p) Subset of ports that are incompatible with each other /Singapore, Osaka/
subIncheVicto(p) Subset of ports that are incompatible with each other /Incheon, Victoria/;

Parameters
*The fleet planning problem we need to solve, has the following characteristics. k describes the minimum number of
*ports that the company has to service. F(v) describes a fixed cost for using a given ship. G(v) indicates how
*many days a given ship can service, and D(p) tells us how many times port p needs to be visited.  
k minimum number of ports the company needs to service /5/
F(v) Costs of using ship v /v1 65, v2 60, v3 92, v4 100, v5 110/
G(v) maximum number of days ship v can service /v1 300, v2 250, v3 350, v4 330, v5 300/
D(p) minimum number of times port p needs to be serviced /Singapore 15, Incheon 18, Shanghai 32, Sydney 32, Gladstone 45, Dalian 32, Osaka  15, Victoria 18/;

*Now we present a table that shows the costs for each ship to sail each route. 
Table
C(v,r) Costs in million dollars for ship v to sail route r
    Asia    ChinaPacific
v1  1.41    1.9
v2  3.0     1.5
v3  0.4     0.8
v4  0.5     0.7
v5  0.7     0.8;

*Now we present a table that shows how many days it will take a given ship to sail a given route. 
Table
T(v,r) Time in days for how long it takes ship v to complete route r
    Asia    ChinaPacific
v1  14.4    21.2
v2  13.0    20.7
v3  14.4    20.6
v4  13.0    19.2
v5  12.0    20.1;

*Now we present a table that indicates whether route r passes through port p.
Table
A(r,p) Indicates whether route r passes through port p
                Singapore   Incheon Shanghai    Sydney  Gladstone   Dalian  Osaka   Victoria
Asia            1           1       1           0       0           1       1       0
ChinaPacific    0           0       1           1       1           1       0       1   ;

*To solve the problem we will need to define some variables. First of all, we define two binary variables.
*The x-variable indicates whether ship v is chosen (1), or not (0). The y-variable indicates whether port p
*is serviced (1), or not. Later we will make sure that these variables take values they are supposed to.
Binary Variables
x(v) Indicates whether ship v is used or not
y(p) Indicates whether port p is serviced or not;

*Next, we define an integer variable, that describes how many times a given ship v sails a given route r. 
Integer variables
w(v,r) describes the number of times ship v sails route r;

*The idea of this problem is to minimize SuperSea's total costs of the service. This is done by finding the right
*ports to visit, combined with using the right routes. We define the objective function value
*by the greek letter xi. 
Variables
xi objective function value;

*By now we can write down the mathematical formulation of our problem. We begin by giving names to
*the objective function and our constraints, and we furthermore indicate what they depend on.
Equations
objectivefunction the objective function
minserviceports minimum number of ports the company needs to service
maxdaysashipcansail(v) maximum number of days ship v can be used for routes
minnumberportmustbeserviced(p) minimum number of times port p must be serviced if chosen
notbothSingaOsaka If Singapore is being serviced then Osaka cannot be serviced and vice versa
notbothIncheVicto If Incheon is being serviced then Victoria cannot be serviced and vice versa
forcingx1(v) first constraint on x forcing it to take the values we want
forcingx2(v) second constraint on x forcing it to take the values we want;

*The objective function indicates that we would like to minimize the total costs for SuperSea. We sum over
*fixed costs for ships in use, and we add costs pr route a given ship sails.
objectivefunction .. xi =e= sum(v, F(v)*x(v))+sum((v,r), C(v,r)*w(v,r));

*SuperSea needs to service at least K ports. We sum y(p) over p, and demand minimum five ports to be serviced.
minserviceports .. sum(p, y(p)) =g= K;

*A ship can sail maximum G(v) days. The time spent on the different routes cannot exceed this value.
maxdaysashipcansail(v) .. sum(r, T(v,r)*w(v,r)) =l= G(v);

*If SuperSea chooses to service port p, then SuperSea has to visit this port at least D(p) times. 
minnumberportmustbeserviced(p) .. sum((r,v), w(v,r)*A(r,p)) =g= D(p)*y(p);

*If SuperSea services Singapore, it cannot service Osaka, and vice versa. The sum of the y(p)'s for p
*in the subset subSingaOsaka can at maximum be one. 
notbothSingaOsaka .. sum(subSingaOsaka(p), y(p)) =l= 1;

*If SuperSea services Incheon, it cannot service Victoria, and vice versa. The sum of the y(p)'s for p
*in the subset subIncheVicto can at maximum be one.
notbothIncheVicto .. sum(subIncheVicto(p), y(p)) =l= 1;

*If ship v is not used for a single route, then we force x(v) to be zero. 
forcingx1(v) .. x(v) =l= sum(r, w(v,r));

*If ship v is used for one or more routes, then we force x(v) to be one.
forcingx2(v) .. x(v)*10000 =g= sum(r, w(v,r));

*We define the optimization problem by all the previous descriptions.
Model fleetplanning /all/;

*We solbe the optimization problem by indicating that we need to maximize xi under the given constraints.
Solve fleetplanning using MIP minimizing xi;

Display xi.l, x.l, y.l, w.l
*The mininal costs for SuperSea is 274.3 million dollars.

